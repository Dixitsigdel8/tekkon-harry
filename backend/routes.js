const router = require('express').Router()
const {addCharacter,getAllCharacter, getCharacterId} = require('./controller/character')

router.post('/api/add', addCharacter)
router.get('/api/characters', getAllCharacter)
router.get('/api/character/:id', getCharacterId)

module.exports = router

