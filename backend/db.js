const mongoose = require('mongoose')



const ConnectionDB = async () => {
  try {
    const URL = process.env.MONGO_URL
    await mongoose.connect(URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
     
    });
    console.log("Database connected successfully");
  } catch (error) {
    console.log("Error: ", error.message);
  }
};

module.exports = ConnectionDB
