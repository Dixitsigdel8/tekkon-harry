const express = require("express")
const app = express()
const ConnectionDB = require("./db")
const Routes = require('./routes')
const cors = require('cors')
require('dotenv').config()

//database connection
ConnectionDB()

//middleware
app.use(express.json())
app.use(cors())

//route
app.use(Routes)

const PORT = process.env.PORT || 8000
app.listen(PORT,()=>{
    console.log(`Server is running at Port ${PORT}`)
})