const mongoose = require('mongoose')

const characterSchema = new mongoose.Schema({
    name: {
        type:String,
        required:true

    },
    dob: {
        type:String,

    },
    birthPlace: {
        type:String,

    },
    nickName: {
        type:String,

    },
    details: {
        type:String,

    },
    img:{
        type: String
    }
    
},{
    timestamps:true
})

module.exports = mongoose.model('Character',characterSchema)