const Character = require("../models/character")

exports.addCharacter = async (req, res) => {
    try {
      const add = await new Character(req.body);
      add.save();
      res.status(200).json({ msg: "Character added successfully" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ msg: "Error while adding Character" });
    }
  };

  exports.getAllCharacter = async (req, res) => {
    try {
        const characters = await Character.find({});
        res.status(200).json({ characters, msg: "Characters listed successfully" });
      } catch (error) {
        console.log(error);
        res.status(500).json({ msg: "Error while listing Characters" });
      }
  };

  exports.getCharacterId = async (req, res) => {
    try {
      const { id } = req.params;
      const character = await Character.findById(id);
      res.status(200).json({ character, msg: "Single Character Listed Successfully. " });
    } catch (error) {
      console.log(error);
      res.status(500).json({ msg: "Error while listing Character" });
    }
  };
  